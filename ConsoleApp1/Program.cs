﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*         Console.WriteLine("Write Xmin");
                       string sxMin = Console.ReadLine();
                       double xMin = Double.Parse(sxMin);
                       Console.WriteLine("Write Xmax");
                       string sxMax = Console.ReadLine();
                       double xMax = double.Parse(sxMax);
                       Console.WriteLine("Write delta x");
                       string sdeltaX = Console.ReadLine();
                       double deltaX = Double.Parse(sdeltaX);
                       Console.WriteLine("****************************");
                       for(;xMin <= xMax ; xMin += deltaX)
                       {
                           Console.WriteLine("x = {0}\t\ty = {1}", Math.Round(xMin, 3), Math.Round(xMin*xMin, 3));
                       }
                       Console.ReadKey();*/
            Console.WriteLine("Write Xmin");
            string sxMin = Console.ReadLine();
            double xMin = Double.Parse(sxMin);
            Console.WriteLine("Write Xmax");
            string sxMax = Console.ReadLine();
            double xMax = double.Parse(sxMax);
            Console.WriteLine("Write delta x");
            string sdeltaX = Console.ReadLine();
            double deltaX = Double.Parse(sdeltaX);
            Console.WriteLine("****************************");
            double sum = 0;
            for (; xMin <= xMax; xMin += deltaX)
            {
                double x2 = xMin * 3;
                double intermedRes = xMin + x2 + Math.Sin(xMin * x2);
                double intermedRes2 = intermedRes / 5 - Math.Cos(x2 * x2);
                double res = Math.Sqrt(56 * xMin + intermedRes2);
                Console.WriteLine(Math.Round(res, 4));
                if (!Double.IsNaN(res))
                {
                    sum += Math.Sin(res);
                }
            }
            Console.WriteLine("Sum of sinus " + Math.Round(sum, 4));
        }
    }
}